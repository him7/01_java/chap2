package com.java.section01;

public class Application01 {

	public static void main(String[] args) {
		/* 임의의 정수를 하나 선언하고 선언한 숫자가 짝수이면 "짝수다",
		 * 짝수가 아니면 홀수다를 출력하세요
		 * 
		 * -- 출력 예시 --
		 * 입력하신 숫자는 홀수다.
		 * */
		
		int num = 124;
		String str = (num%2==0)?"짝수":"홀수";
		System.out.println("num은 "+str);
		
	}

}
