package com.java.section01;

public class Application02 {

	public static void main(String[] args) {
		/* 임의의 나이를 정수로 선언하고 어린이(13세 이하)인지, 청소년(13세 초과 ~ 19세 이하)
		 * 인지 성인(19세 초과)인지 출력하세요.
		 * 
		 * -- 출력 예시--
		 * 
		 * 청소년
		 * */
		
		int age = 19;
		String str = (age<13)?"어린이":(age<19)?"청소년":"성인";
		System.out.println(age+"는 "+str);
	}

}
